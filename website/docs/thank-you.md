---
title: Thank you!
---

Thank you for reading! If you are a fellow *Technical Writer*, I do hope you found some useful resources here.

I had a great time customizing the **Docusaurus** template to make this site. The original document included the following resources that might be useful to you as well:

## What's next?

- [Read the official documentation](https://v2.docusaurus.io/).
- [Design and Layout your Docusaurus site](https://v2.docusaurus.io/docs/styling-layout)
- [Integrate a search bar into your site](https://v2.docusaurus.io/docs/search)
- [Find inspirations in Docusaurus showcase](https://v2.docusaurus.io/showcase)
- [Get involved in the Docusaurus Community](https://v2.docusaurus.io/community/support)
