module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Online Documentation',
      items: [
        'using-markdown',
        'ubuntu-console',
        'create-git-project',
        'clone-repo',
        'vsc',
      ],
    },
    {
      type: 'category',
      label: 'Problems?',
      items: [
        'update-node',
      ],
    },
    {
      type: 'category',
      label: 'Using This Template',
      items: [
        'getting-started',
        'create-document',
        'create-page',
        'create-blog-post',
      ],
    },
    {
      type: 'category',
      label: 'Romina',
      items: [
        'my-personal-links',
        'cv',
        'thank-you',
      ],
    },
  ],
};
