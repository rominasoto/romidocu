import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Welcome',
    imageUrl: 'img/romidocu-welcome.png',
    description: (
      <>
        Welcome to my Online Documentation Porfolio. This documentation hub aims at showcasing my work, as well as gathering useful resources and step-by-step help for others looking to make their own documentation portoflios.
      </>
    ),
  },
  {
    title: 'Why online documentation?',
    imageUrl: 'img/whyonline.png',
    description: (
      <>
        It's dynamic, portable, and easy to consult. As a Technical Writer, it requires learning a number of skills that will increase your professional knowledge and capabilities.
      </>
    ),
  },
  {
    title: 'Working with Agile?',
    imageUrl: 'img/agile.png',
    description: (
      <>
        Why not make your documentation Agile as well, then? Updates can follow the sprint rather than waiting for a new User Manual version to come out. Moreover, Git based documentation offers great traceability without extra work, through the history feature of the repository.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Access Documents
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}
