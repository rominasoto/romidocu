/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Romina\'s Portfolio',
  tagline: 'an Agile documentation hub example',
  url: 'https://pages.gitlab.io',
  baseUrl: '/romidocu/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/romidocu-logo.ico',
  organizationName: 'rominasoto', // Usually your GitHub org/user name.
  projectName: 'romidocu', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'RomiDocu on GitLab Pages',
      logo: {
        alt: 'My Site Logo',
        src: 'img/romidocu-logo.png',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {
          to: 'docs/cv',
          activeBasePath: 'cv',
          label: 'CV',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/pages/docusaurus',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Using Markdown',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Contact the creators of this template',
          items: [
            {
              label: 'Original Project Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
            {
              label: 'Original Project Discord',
              href: 'https://discordapp.com/invite/docusaurus',
            },
            {
              label: 'Original Project Twitter',
              href: 'https://twitter.com/docusaurus',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'Original Project GitHub',
              href: 'https://github.com/facebook/docusaurus',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
