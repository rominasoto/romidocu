---
slug: getting-started
title: Getting Started
author: Romina Soto
author_title: Technical Writer
author_url: https://gitlab.com/rominasoto
author_image_url: https://assets.gitlab-static.net/uploads/-/system/user/avatar/9037682/avatar.png
tags: [hello, getting started, romidocu]
---

This is my first post here, and it's a test! I'm customizing the amazing <a href="https://github.com/facebook/docusaurus" target="_blank">Docusaurus</a>  template to create an online portfolio for my documentation.

Find the links to the original project repo and social pages on the homepage of this documentation hub.

Bye for now!
